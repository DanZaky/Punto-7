Pregunta 7

Elabore una aplicación web (en un lenguaje de su preferencia) en la cual se visualice un mapa de Bogotá (centrado inicialmente en el edificio de ESRI COLOMBIA en el Chicó) y en el que cada vez que se hace click en un lugar del mapa, se muestre un pop-up con la dirección del sitio.

Daniel Arturo Saenz Quintero

Universidad Distrital Francisco José de Caldas
